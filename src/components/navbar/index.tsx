import "../../styles/navbar.css";
import { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { NavLink } from "react-router";

const Navbar = () => {
  const [isMobileMenuOpen, setMobileMenuOpen] = useState(false);

  const toggleMobileMenu = () => {
    setMobileMenuOpen(!isMobileMenuOpen);
  };

  return (
    <nav className="navbar">
      <div className="logo">Portfolio</div>
      <div
        className={`nav-links ${isMobileMenuOpen ? "mobile-menu-open" : ""}`}
      >
        <ul>
          <li>
            <NavLink to={"/"}>Home</NavLink>
          </li>
          <li>
            <NavLink to={"/about"}>About</NavLink>
          </li>
          <li>
            <NavLink to={"/skill"}>Skill</NavLink>
          </li>
          <li>
            <NavLink to={"/project"}>Work</NavLink>
          </li>
          <li>
            <NavLink to={"/services"}>Services</NavLink>
          </li>
        </ul>
      </div>
      <div className="mobile-menu-button" onClick={toggleMobileMenu}>
        {isMobileMenuOpen ? (
          <FontAwesomeIcon icon={"fa-solid fa-x" as any} />
        ) : (
          <FontAwesomeIcon icon={"fa-solid fa-grip-vertical" as any} />
        )}
      </div>
    </nav>
  );
};

export default Navbar;
