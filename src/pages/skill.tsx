import "@/styles/skill.css";

export default function Skill() {
  return (
    <div className="h-screen flex">
      <div className="bg-white mx-10 m-auto md:mx-32 w-full shadow-[0px_20px_20px_10px_#00000024] rounded-md">
        <div className="skill1">
          My Skill<span className="dot">.</span>
        </div>
        <div className="gap-2 items-center py-8 px-4 mx-auto max-w-screen-xl md:grid md:grid-cols-2 sm:py-16 lg:px-6">
          <div>
            {/*  */}
            <div>
              <span className="skill2">React JS & React Framework</span>
              <div className="mb-5 h-2 overflow-hidden rounded-full bg-indigo-200">
                <div
                  className="h-2 animate-pulse rounded-full bg-indigo-900"
                  style={{ width: "88%" }}
                >
                  <div className="h-full w-full translate-x-full transform bg-white"></div>
                </div>
              </div>
            </div>
            {/*  */}
            {/*  */}
            <div>
              <span className="skill2">React Native & Expo</span>
              <div className="mb-5 h-2 overflow-hidden rounded-full bg-blue-200">
                <div
                  className="h-2 animate-pulse rounded-full bg-blue-900"
                  style={{ width: "89%" }}
                >
                  <div className="h-full w-full translate-x-full transform bg-white"></div>
                </div>
              </div>
            </div>
            {/*  */}
            {/*  */}
            <div>
              <span className="skill2">Vue.Js & Nuxt</span>
              <div className="mb-5 h-2 overflow-hidden rounded-full bg-green-200">
                <div
                  className="h-2 animate-pulse rounded-full bg-green-900"
                  style={{ width: "85%" }}
                >
                  <div className="h-full w-full translate-x-full transform bg-white"></div>
                </div>
              </div>
            </div>
            {/*  */}
            {/*  */}
            <div>
              <span className="skill2">SCSS</span>
              <div className="mb-5 h-2 overflow-hidden rounded-full bg-pink-200">
                <div
                  className="h-2 animate-pulse rounded-full bg-red-800"
                  style={{ width: "90%" }}
                >
                  <div className="h-full w-full translate-x-full transform bg-white"></div>
                </div>
              </div>
            </div>
            {/*  */}
          </div>
          <div>
            {/* ROW 2 */}
            <div>
              <span className="skill2">Python</span>
              <div className="mb-5 h-2 overflow-hidden rounded-full bg-yellow-200">
                <div
                  className="h-2 animate-pulse rounded-full bg-yellow-800"
                  style={{ width: "70%" }}
                >
                  <div className="h-full w-full translate-x-full transform bg-white"></div>
                </div>
              </div>
            </div>
            <div>
              <span className="skill2">Node JS & Framework</span>
              <div className="mb-5 h-2 overflow-hidden rounded-full bg-green-200">
                <div
                  className="h-2 animate-pulse rounded-full bg-teal-800"
                  style={{ width: "90%" }}
                >
                  <div className="h-full w-full translate-x-full transform bg-white"></div>
                </div>
              </div>
            </div>
            <div>
              <span className="skill2">PHP & Framework</span>
              <div className="mb-5 h-2 overflow-hidden rounded-full bg-cyan-800">
                <div
                  className="h-2 animate-pulse rounded-full bg-cyan-800"
                  style={{ width: "90%" }}
                >
                  <div className="h-full w-full translate-x-full transform bg-white"></div>
                </div>
              </div>
            </div>
            <div>
              <span className="skill2">Figma UI</span>
              <div className="mb-5 h-2 overflow-hidden rounded-full bg-cyan-200">
                <div
                  className="h-2 animate-pulse rounded-full bg-slate-800"
                  style={{ width: "90%" }}
                >
                  <div className="h-full w-full translate-x-full transform bg-white"></div>
                </div>
              </div>
            </div>
            {/*  */}
            {/* END ROW */}
          </div>
        </div>
      </div>
    </div>
  );
}
