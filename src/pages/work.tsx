import "@/styles/project.css";

export default function Project() {
  return (
    <div className="h-screen flex mt-20 mb-20">
      <div className="bg-white m-auto shadow-[0px_20px_20px_10px_#00000024] md:w-1/2 rounded-md">
        <div className="skill1">
          Work Experience<span className="dot">.</span>
        </div>

        {/* component */}
        <div className="w-10/12 md:w-7/12 lg:6/12 mx-auto relative">
          <div className="border-l-2 mt-10">
            {/* Card 1 */}
            <div className="transform transition cursor-pointer hover:-translate-y-2 ml-10 relative flex items-center px-6 py-4 bg-orange-600 text-white rounded mb-10 flex-col md:flex-row space-y-4 md:space-y-0">
              {/* Dot Follwing the Left Vertical Line */}
              <div className="w-5 h-5 bg-orange-600 absolute -left-10 transform -translate-x-2/4 rounded-full z-10 mt-2 md:mt-0" />
              {/* Line that connecting the box with the vertical line */}
              <div className="w-10 h-1 bg-orange-300 absolute -left-10 z-0" />
              {/* Content that showing in the box */}
              <div className="flex-auto">
                <h1 className="text-xs">Apr 2023 - Present</h1>
                <h1 className="text-xl font-bold">BTPN Syariah</h1>
                <h3>Front-end Developer</h3>
              </div>
            </div>
            {/* Card 2 */}
            <div className="transform transition cursor-pointer hover:-translate-y-2 ml-10 relative flex items-center px-6 py-4 bg-blue-600 text-white rounded mb-10 flex-col md:flex-row space-y-4 md:space-y-0">
              {/* Dot Follwing the Left Vertical Line */}
              <div className="w-5 h-5 bg-blue-600 absolute -left-10 transform -translate-x-2/4 rounded-full z-10 mt-2 md:mt-0" />
              {/* Line that connecting the box with the vertical line */}
              <div className="w-10 h-1 bg-blue-300 absolute -left-10 z-0" />
              {/* Content that showing in the box */}
              <div className="flex-auto">
                <h1 className="text-xs">Feb 2022 - May 2023</h1>
                <h1 className="text-xl font-bold">
                  PT. Onglai Aneka Toolindo
                </h1>
                <h3>Lead Front-end</h3>
              </div>
            </div>
            {/* Card 3 */}
            <div className="transform transition cursor-pointer hover:-translate-y-2 ml-10 relative flex items-center px-6 py-4 bg-blue-600 text-white rounded mb-10 flex-col md:flex-row space-y-4 md:space-y-0">
              {/* Dot Follwing the Left Vertical Line */}
              <div className="w-5 h-5 bg-blue-600 absolute -left-10 transform -translate-x-2/4 rounded-full z-10 mt-2 md:mt-0" />
              {/* Line that connecting the box with the vertical line */}
              <div className="w-10 h-1 bg-blue-300 absolute -left-10 z-0" />
              {/* Content that showing in the box */}
              <div className="flex-auto">
                <h1 className="text-xs">Oct 2021 - Feb 2022</h1>
                <h1 className="text-xl font-bold">
                  PT. Onglai Aneka Toolindo
                </h1>
                <h3>Full-stack Developer</h3>
              </div>
            </div>
            {/* Card 4 */}
            <div className="transform transition cursor-pointer hover:-translate-y-2 ml-10 relative flex items-center px-6 py-4 bg-purple-600 text-white rounded mb-10 flex-col md:flex-row space-y-4 md:space-y-0">
              {/* Dot Follwing the Left Vertical Line */}
              <div className="w-5 h-5 bg-purple-600 absolute -left-10 transform -translate-x-2/4 rounded-full z-10 mt-2 md:mt-0" />
              {/* Line that connecting the box with the vertical line */}
              <div className="w-10 h-1 bg-purple-300 absolute -left-10 z-0" />
              {/* Content that showing in the box */}
              <div className="flex-auto">
                <h1 className="text-xs">Aug 2019 - Aug 2021</h1>
                <h1 className="text-xl font-bold">
                  Jakarta Sembilan indonesia
                </h1>
                <h3>IoT Developer</h3>
              </div>
            </div>
            {/* Card 5 */}
            <div className="transform transition cursor-pointer hover:-translate-y-2 ml-10 relative flex items-center px-6 py-4 bg-yellow-600 text-white rounded mb-10 flex-col md:flex-row">
              {/* Dot Follwing the Left Vertical Line */}
              <div className="w-5 h-5 bg-yellow-600 absolute -left-10 transform -translate-x-2/4 rounded-full z-10 -mt-2 md:mt-0" />
              {/* Line that connecting the box with the vertical line */}
              <div className="w-10 h-1 bg-yellow-300 absolute -left-10 z-0" />
              {/* Content that showing in the box */}
              <div className="flex-auto">
                <h1 className="text-xs">Aug 2017 - Jul 2019</h1>
                <h1 className="text-xl font-bold">
                  PT. Kharisma Perkasa Mandiri
                </h1>
                <h3>Full-stack Developer</h3>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
