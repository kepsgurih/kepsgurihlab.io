import { Badge } from "@/components/ui/badge";
import { Card, CardContent, CardHeader, CardTitle } from "@/components/ui/card";
import { Skeleton } from "@/components/ui/skeleton";
import { GET_SERVICES } from "@/services/query";
import { useQuery } from "@apollo/client";

interface serviceProps {
    title: string
    description: string
    badge: string[]
}

export default function Services() {
    const { data, loading } = useQuery(GET_SERVICES)
    return (
        <section>
            <div className="py-8 px-4 mx-auto max-w-screen-xl lg:py-16 lg:px-6">
                <div className="mx-auto max-w-screen-sm text-center lg:mb-16 mb-8">
                    <h2 className="mb-4 text-3xl lg:text-4xl tracking-tight font-extrabold text-gray-900 dark:text-white">Services</h2>
                    <p className="font-light text-gray-500 sm:text-xl dark:text-gray-400">We use an agile approach to test assumptions and connect with the needs of your audience early and often.</p>
                </div>
                <div className="grid gap-8 lg:grid-cols-2">
                    {
                        loading ?
                            (
                                <>
                                    <Skeleton className="w-full h-40" />
                                    <Skeleton className="w-full h-40" />
                                    <Skeleton className="w-full h-40" />
                                    <Skeleton className="w-full h-40" />
                                </>
                            )
                            :

                            data.Services.map((item: serviceProps) => {
                                return (
                                    <Card>
                                        <CardHeader>
                                            <div className="flex space-x-2 items-center mb-5 text-gray-500">
                                                {
                                                    item.badge.map((badg: string) => {
                                                        return(
                                                            <Badge variant={'outline'}>{badg}</Badge>
                                                        )
                                                    })
                                                }
                                            </div>
                                            <CardTitle>{item.title}</CardTitle>
                                        </CardHeader>
                                        <CardContent>
                                            <p className="mb-5 font-light text-gray-500 dark:text-gray-400">
                                                {item.description}
                                            </p>
                                        </CardContent>
                                    </Card>
                                )
                            })


                    }
                </div>
            </div>
        </section>
    )
}