import './App.css'
import { Route, Routes } from "react-router";
import { lazy, Suspense } from "react";

const Layout = lazy(() => import('./layouts'));
const Hero = lazy(() => import("./pages/hero"));
const About = lazy(() => import("./pages/about"));
const Skill = lazy(() => import("./pages/skill"));
const Project = lazy(() => import("./pages/work"));
const Services = lazy(() => import("./pages/services"));

const LoadingFallback = () => (
  <div className="flex items-center justify-center min-h-screen">
    <div>Loading...</div>
  </div>
);

function App() {
  return (
    <Suspense fallback={<LoadingFallback />}>
      <Routes>
        <Route path="/" element={
          <Suspense fallback={<LoadingFallback />}>
            <Layout />
          </Suspense>
        }>
          <Route index element={
            <Suspense fallback={<LoadingFallback />}>
              <Hero />
            </Suspense>
          } />
          <Route path="about" element={
            <Suspense fallback={<LoadingFallback />}>
              <About />
            </Suspense>
          } />
          <Route path="skill" element={
            <Suspense fallback={<LoadingFallback />}>
              <Skill />
            </Suspense>
          } />
          <Route path="project" element={
            <Suspense fallback={<LoadingFallback />}>
              <Project />
            </Suspense>
          } />
          <Route path="services" element={
            <Suspense fallback={<LoadingFallback />}>
              <Services />
            </Suspense>
          } />
        </Route>
      </Routes>
    </Suspense>
  );
}

export default App;