import "@/styles/about.css";
import abstract from "@/assets/abstract.svg";
import { useQuery } from "@apollo/client";
import { GET_DASHBOARD } from "@/services/query";

export default function About() {
  const { data, loading } = useQuery(GET_DASHBOARD);
  return (
    <div id="about" className="px-20 flex h-screen">
      <div className="m-auto">
        <div className="text-center about1">
          About Me<span className="dot">.</span>
        </div>
        {loading ? (
          <>
            <div className='w-full bg-gray-400 h-4 animate animate-pulse mb-2' />
            <div className='w-full bg-gray-400 h-4 animate animate-pulse mb-2' />
            <div className='w-full bg-gray-400 h-4 animate animate-pulse mb-2' />
            <div className='w-3/4 bg-gray-400 h-4 animate animate-pulse mb-4' />
          </>

        )
          :
          <div className="about2 md:px-40" dangerouslySetInnerHTML={{ __html: data?.config_dashboard[0]?.aboutMe }} />
        }
        <div
          className="absolute w-full md:top-80 top-100 "
          style={{ zIndex: -99 }}
        >
          <img src={abstract} alt="bg" className="mx-auto w-2/4" />
        </div>
      </div>
    </div>
  );
}
