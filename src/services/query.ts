import { gql } from '@apollo/client';

export const GET_DASHBOARD = gql`query showDashboard {
  config_dashboard(limit: 1) {
    aboutWeb
    cv_link
    cv_text
    description
    fullName
    social
    titleWeb
    aboutMe
  }
}
`;

export const GET_SERVICES = gql`query getServices {
  Services {
    title
    description
    badge
  }
}
`;