import "./App.css";
import Navbar from "./components/navbar";
import accaro from './assets/accaro.svg'
import vec from "./assets/vec1.png";
import { Outlet } from "react-router";

function Layout() {
  return (
    <div>
      <Navbar />
      <div className="absolute -z-50">
        <img src={accaro} alt="bg1" />
      </div>
      <div
        className="absolute top-10 h-full"
        style={{
          marginTop: 100,
          marginLeft: 200,
          zIndex: -99,
        }}
      >
        <img src={vec} alt="bg1" />
      </div>
      <Outlet />
    </div>
  );
}

export default Layout;
