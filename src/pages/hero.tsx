import '@/styles/hero.css'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button } from "@/components/ui/button";
import { useQuery } from '@apollo/client';
import { GET_DASHBOARD } from '@/services/query';

interface SocialProps {
  name: string
  link: string
}

export default function Hero() {
  const { data, loading } = useQuery(GET_DASHBOARD);
  const LoaderContent = () => {
    return (
      <>
        <div className='w-24 bg-gray-400 h-4 animate animate-pulse mb-2' />
        <div className='w-1/2 bg-red-400 h-8 animate animate-pulse mb-2' />
        <div className='w-1/2 bg-gray-400 h-12 animate animate-pulse mb-4' />
        <div className='w-1/2 bg-gray-400 h-4 animate animate-pulse mb-2' />
        <div className='w-full bg-gray-400 h-4 animate animate-pulse mb-2' />
        <div className='w-full bg-gray-400 h-4 animate animate-pulse mb-2' />
        <div className='w-full bg-gray-400 h-4 animate animate-pulse mb-2' />
        <div className='w-3/4 bg-gray-400 h-4 animate animate-pulse mb-4' />

        <div className='flex space-x-4'>
          <div className='w-4 h-4 bg-gray-400 animate animate-pulse' />
          <div className='w-4 h-4 bg-gray-400 animate animate-pulse' />
          <div className='w-4 h-4 bg-gray-400 animate animate-pulse' />
          <div className='w-4 h-4 bg-gray-400 animate animate-pulse' />
        </div>
        <div className='mt-8'>
          <Button variant={'destructive'} disabled>
            Download
          </Button>
        </div>
      </>
    )
  }

  const LoadedContent = () => {
    return (
      <>
        <h2 className="text1">
          {data?.config_dashboard[0]?.aboutWeb}
        </h2>
        <h2 className="text2 ">{data?.config_dashboard[0]?.fullName}</h2>
        <h2 className="job1">{data?.config_dashboard[0]?.titleWeb}</h2>
        <p className="md:mr-40" dangerouslySetInnerHTML={{
          __html:
            data?.config_dashboard[0]?.description
        }} />
        <div className="sosmed">
          <ul>
            {
              data?.config_dashboard[0]?.social.map((item: SocialProps) => {
                return (
                  <li>
                    <a
                      target="_blank"
                      rel="noreferrer"
                      href={item.link}
                    >
                      <FontAwesomeIcon icon={item.name as any} />
                    </a>
                  </li>
                )
              })
            }
          </ul>
        </div>
        <div className="mt-10">
          <Button variant={'destructive'} asChild>
            <a
              target="_blank"
              rel="noreferrer"
              href={data?.config_dashboard[0]?.cv_link}>
              {data?.config_dashboard[0]?.cv_text}
            </a>

          </Button>
        </div>
      </>
    )
  }

  return (
    <section id="home">
      <div className="gap-2 items-center py-8 px-4 mx-auto max-w-screen-xl md:grid md:grid-cols-2 sm:py-16 lg:px-6">
        <div className="mt-4 md:mt-0 w-full">
          {
            loading ? (<LoaderContent />)
              :
              <LoadedContent />
          }

          {/* {loaderContent()} */}

        </div>
        <div className="justify-center hidden md:block">
          <img
            className="w-1/2 mx-auto "
            src={
              "https://firebasestorage.googleapis.com/v0/b/port-321ab.appspot.com/o/cv%2Fmyface.png?alt=media"
            }
            alt="ava"
          />
        </div>
      </div>
    </section>
  );
}
